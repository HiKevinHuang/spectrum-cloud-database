# config file
DEBUG = True
IP = '140.118.118.126'
Port = '5000'
WTF_CSRF_ENABLED = False
SECRET_KEY = 'any secret string'
WTF_CSRF_SECRET_KEY = 'a csrf secret key'
DB_CONNECTION_STRING = 'mysql+mysqlconnector://root:@localhost:3306/python_web_demo'