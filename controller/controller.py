from flask import Flask, request, redirect, url_for, render_template, current_app
from flask import jsonify
from controller import blueprint
import json
import base64
from model.DBquery import *
from model.WhiteBalance import WB
# from model.EncodeType import test
# from model.DBquery import test

@blueprint.route('/', methods=['POST', 'GET'])
def index():
    return render_template("index.html")


@blueprint.route('/get_all_image_name', methods=['POST'])
def get_all_image_name():
    # 格式為Tuple
    result = show_all_image_name()
    image_name = []
    for i in range(len(result)):
        image_name.append(result[i][0])
    image_name = json.dumps(image_name)

    return image_name


@blueprint.route('/test', methods=['POST', 'GET'])
def index_copy():
    return render_template("index_copy.html")


@blueprint.route('/query', methods=['POST'])
def query():
    light_source = request.values.get('light_source')
    image_name = request.values.get('image_name')
    image_degree = request.values.get('degree')
    # 取得7張頻譜圖片
    image_400nm, image_450nm, image_500nm, image_550nm, image_600nm, image_650nm, image_700nm = show_spectrum_image(image_name, image_degree)
    
    status = ""
    if (len(image_400nm) or len(image_450nm) or len(image_500nm) or len(image_550nm) or len(image_600nm) or len(image_650nm) or len(image_700nm)) != 0:
        # 轉換圖片格式 (base64)
        image_400nm_convert = base64.b64decode(image_400nm)
        image_450nm_convert = base64.b64decode(image_450nm)
        image_500nm_convert = base64.b64decode(image_500nm)
        image_550nm_convert = base64.b64decode(image_550nm)
        image_600nm_convert = base64.b64decode(image_600nm)
        image_650nm_convert = base64.b64decode(image_650nm)
        image_700nm_convert = base64.b64decode(image_700nm)
        
        # 確認光源
        if light_source == 'D50' or light_source == 'D65' or light_source == 'D75' or light_source == 'A' or light_source == 'E':
            image_original, image_light_convert = WB(image_400nm_convert, image_450nm_convert, image_500nm_convert, image_550nm_convert, image_600nm_convert, image_650nm_convert, image_700nm_convert, light_source)  # get binary image
            # 解碼成Base64
            image_original = base64.b64encode(image_original)
            image_light_convert = base64.b64encode(image_light_convert)
            print("產生成功 !")
            data_exist = check_data_exist(image_name)
            print("是否已有資料: ", data_exist)
            if data_exist == 'none':
                # 將光源轉換後的圖片新增到資料庫
                mode = 'insert'
                insert_light_convert_image(mode, image_name, image_degree, light_source, str(image_original)[2:-1], str(image_light_convert)[2:-1])
            if data_exist == 'exist':
                # 更新資料庫中光源轉換後的圖片
                mode = 'update'
                insert_light_convert_image(mode, image_name, image_degree, light_source, str(image_original)[2:-1], str(image_light_convert)[2:-1])
            # 編碼成網頁看得懂的base64
            image_original = "data:image/jpg;base64," + str(image_original)[2:-1]
            image_light_convert = "data:image/jpg;base64," + str(image_light_convert)[2:-1]
            # 各頻譜原圖
            image_400nm = "data:image/jpg;base64," + str(image_400nm)[2:-1]
            image_450nm = "data:image/jpg;base64," + str(image_450nm)[2:-1]
            image_500nm = "data:image/jpg;base64," + str(image_500nm)[2:-1]
            image_550nm = "data:image/jpg;base64," + str(image_550nm)[2:-1]
            image_600nm = "data:image/jpg;base64," + str(image_600nm)[2:-1]
            image_650nm = "data:image/jpg;base64," + str(image_650nm)[2:-1]
            image_700nm = "data:image/jpg;base64," + str(image_700nm)[2:-1]
            status = "Success"
        else:
            print("NULL")
            status = "ERROR"
    else:
        status = "Failure"
        image_400nm = ''
        image_450nm = ''
        image_500nm = ''
        image_550nm = ''
        image_600nm = ''
        image_650nm = ''
        image_700nm = ''
        image_original = ''
        image_light_convert = ''

    data = [{
            'status':status,
            'image_400nm_original':image_400nm,
            'image_450nm_original':image_450nm,
            'image_500nm_original':image_500nm,
            'image_550nm_original':image_550nm,
            'image_600nm_original':image_600nm,
            'image_650nm_original':image_650nm,
            'image_700nm_original':image_700nm,
            'image_original':image_original,
            'image_light_convert':image_light_convert
            }]
    data = json.dumps(data)

    return data


@blueprint.route('/uploadr', methods=['POST', 'GET'])
def uploadr():
    return render_template("uploadr.html")


@blueprint.route('/spectrum_analysis', methods=['POST', 'GET'])
def spectrum_analysis():
    return render_template("spectrum_analysis.html")


@blueprint.route('/line_chart', methods=['POST', 'GET'])
def line_chart():
    return render_template("line_chart.html")


@blueprint.route('/AddData', methods=['POST'])
def AddData():
    image_name = request.values.get("image_name")
    image_degree = request.values.get("degree")
    image_400nm = request.values.get("400nm")
    image_450nm = request.values.get("450nm")
    image_500nm = request.values.get("500nm")
    image_550nm = request.values.get("550nm")
    image_600nm = request.values.get("600nm")
    image_650nm = request.values.get("650nm")
    image_700nm = request.values.get("700nm")
    mode = request.values.get("mode")
    status = upload_files(image_name, image_degree, image_400nm, image_450nm, image_500nm, image_550nm, image_600nm, image_650nm, image_700nm, mode)

    return status


# # 語法、功能測試
# @blueprint.route('/testpost', methods=['POST'])
# def testpost():
#     spectrum_band = request.values.get("spectrum_band")
#     # print(spectrum_band)
#     status = test(spectrum_band)
#     return status
