import pymysql
import sys


# # 語法、功能測試
# def test(spectrum_band):
#     conn, cursor = connection()
#     sql = "SELECT spectrum_band FROM spectrum WHERE spectrum_band = %s"
#     status = ''
#     try:
#         cursor.execute(sql, spectrum_band)
#         data = cursor.fetchone()
#         if data is None:
#             status = "資料不存在"
#     except:
#         print ("Error: unable to fetch data")
#         conn.rollback()

#     cursor.close()
#     conn.close()

#     return status


# mysql連線
def connection():
    try:
        conn = pymysql.connect(host='localhost',port = 3306,user='root',passwd='',db='spectrum_db')
        cursor = conn.cursor()
        print("資料庫連接成功")
    except Exception as e:
        print("資料庫連接失敗:" + str(e))

    return conn, cursor


# 取得資料庫已存在的圖片名稱
def show_all_image_name():
    conn, cursor = connection()
    sql = "SELECT DISTINCT name FROM spectrum;"
    cursor.execute(sql)
    data = cursor.fetchall()
    cursor.close()
    conn.close()

    return data


# 上傳圖片
def upload_files(image_name, image_degree, image_400nm, image_450nm, image_500nm, image_550nm, image_600nm, image_650nm, image_700nm, mode):
    conn, cursor = connection()
    status = ''
    degree = str(image_degree) + "_degree"
    check_data = "SELECT " + degree + " FROM spectrum WHERE name = %s"
    insert_sql = "INSERT INTO spectrum (name, " + degree + ", spectrum_band) VALUES (%s, %s, %s);"
    insert_val = ((image_name, image_400nm, 400),
                (image_name, image_450nm, 450),
                (image_name, image_500nm, 500),
                (image_name, image_550nm, 550),
                (image_name, image_600nm, 600),
                (image_name, image_650nm, 650),
                (image_name, image_700nm, 700))
    update_sql = "UPDATE spectrum SET " + degree + "= %s WHERE name = %s AND spectrum_band = %s"
    update_val = ((image_400nm, image_name, 400),
                (image_450nm, image_name, 450),
                (image_500nm, image_name, 500),
                (image_550nm, image_name, 550),
                (image_600nm, image_name, 600),
                (image_650nm, image_name, 650),
                (image_700nm, image_name, 700))
    if (mode == 'Add'):
        try:
            cursor.execute(check_data, image_name)
            data = cursor.fetchone()
            if data is None:
                print("新增資料")
                cursor.executemany(insert_sql, insert_val)
                # 如果資料庫沒有設定自動提交,這裡要提交一下
                conn.commit()
                status = "Success"
            elif len(data[0]) == 0:
                print("更新資料")
                cursor.executemany(update_sql, update_val)
                conn.commit()
                status = "Success"
            else:
                print("資料已經存在")
                status = "資料已經存在，是否要覆蓋資料 ?"

        except Exception:
            print("資料庫發生異常: ", Exception)
            status = "Failure"

        finally:
            # 關閉資料庫連線
            cursor.close()
            conn.close()
    elif (mode == 'Update'):
        print("新增資料")
        cursor.executemany(update_sql, update_val)
        # 如果資料庫沒有設定自動提交,這裡要提交一下
        conn.commit()
        status = "Success"

        # 關閉資料庫連線
        cursor.close()
        conn.close()

    return status


# 檢查資料是否存在
def check_data_exist(image_name):
    conn, cursor = connection()
    sql = "SELECT name FROM light_convert WHERE name = '{}'".format(image_name)
    cursor.execute(sql)
    result = cursor.fetchone()

    if result is None:
        status = "none"
    else:
        status = "exist"

    cursor.close()
    conn.close()

    return status


# 顯示7頻譜圖片
def show_spectrum_image(image_name, image_degree):
    conn, cursor = connection()
    image_degree = str(image_degree) + "_degree"
    sql = "SELECT {} FROM spectrum WHERE name = '{}' ORDER BY spectrum_band ASC".format(image_degree, image_name)
    try:
        cursor.execute(sql)
        image = cursor.fetchall()
        image_400nm = image[0][0]
        image_450nm = image[1][0]
        image_500nm = image[2][0]
        image_550nm = image[3][0]
        image_600nm = image[4][0]
        image_650nm = image[5][0]
        image_700nm = image[6][0]

    except Exception:
        print("資料庫發生異常: ", Exception)
        image_400nm = ''
        image_450nm = ''
        image_500nm = ''
        image_550nm = ''
        image_600nm = ''
        image_650nm = ''
        image_700nm = ''

    finally:
        # 關閉資料庫連線
        cursor.close()
        conn.close()

    return image_400nm, image_450nm, image_500nm, image_550nm, image_600nm, image_650nm, image_700nm


# 將光源轉換後的圖片存到資料庫
def insert_light_convert_image(mode, image_name, image_degree, light_source, image_original, image_light_convert):
    conn, cursor = connection()
    if mode == 'insert':
        print("插入資料中...")
        insert_sql = "INSERT INTO light_convert (name, degree, original, " + light_source + ") VALUES (%s, %s, %s, %s);"
        insert_val = (image_name, image_degree, image_original, image_light_convert)
        cursor.execute(insert_sql, insert_val)
    elif mode == 'update':
        print("更新資料中...")
        update_sql = "UPDATE light_convert SET original=%s, " + light_source + "=%s WHERE name=%s AND degree=%s;"
        update_val = (image_original, image_light_convert, image_name, image_degree)
        cursor.execute(update_sql, update_val)
    else:
        print("無法判斷")
    conn.commit()
    cursor.close()
    conn.close()
    print("儲存成功 !")


# 顯示原圖、光源轉換圖片
def show_light_convert_image(image_name, image_degree, light_source):
    conn, cursor = connection()
    sql = "SELECT original, {} FROM light_convert WHERE name = '{}' AND degree = {}".format(light_source, image_name, image_degree)
    cursor.execute(sql)
    image = cursor.fetchall()
    original_image = image[0][0]
    light_convert_image = image[0][1]
    cursor.close()
    conn.close()

    return original_image, light_convert_image