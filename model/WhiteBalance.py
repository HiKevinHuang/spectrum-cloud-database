import numpy as np
import cv2
import base64
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import os

# XYZ矩陣轉RGB
def XYZ2RGB(matrix,light=[0.954,1,1.0889],overflow=1):
    h,w,c = matrix.shape
    im_XYZ =np.zeros([h,w,c])+matrix
    matrix[:,:,0]/=matrix[:,:,0].max()*overflow
    im_XYZ[:,:,0] = matrix[:,:,0]*light[0]
    matrix[:,:,1]/=matrix[:,:,1].max()*overflow
    im_XYZ[:,:,1] = matrix[:,:,1]*light[1]
    matrix[:,:,2]/=matrix[:,:,2].max()*overflow
    im_XYZ[:,:,2] = matrix[:,:,2]*light[2]
    im_XYZ /= light[1]
    im_out = np.dot(im_XYZ,XYZ2RGB_matrix().T)
    im_out[im_out<0] = 0
    im_out = im_out**(np.ones(im_XYZ.shape)/2.2)
    im_out /= overflow
    im_out[im_out>1] = 1
    return (im_out*255).astype(np.uint8)
''' 
def XYZ2RGB(matrix,light):    
    matrix1 = np.zeros(matrix.shape)
    matrix1 += matrix
    im_out = np.zeros(matrix.shape)
    light_RGB = np.zeros(light.shape)
    matrix1 *= light
    im_out =np.dot(matrix1,XYZ2RGB_matrix().T)
    light_RGB = np.dot(XYZ2RGB_matrix(),light)
    im_out = RGB2positive(im_out)
    im_out /= light_RGB
    im_out /= min(im_out[:,:,0].max(),im_out[:,:,1].max(),im_out[:,:,2].max())*.8
    im_out *= light_RGB
    pos_overflow = im_out>1
    im_out[pos_overflow] = 1
    result = np.rint(im_out*255).astype(np.uint8)

    return result
'''

def XYZ2RGB_matrix():
    return np.array([[+2.74547,-1.13581,-0.43503],
                    [-0.96927,+1.87601,+0.04469],
                    [+0.01127,-0.11398,+1.01325]])


def RGB2positive(matrix): 
    # 找到B為負值的地方，調整成正值
    matrix_out = np.zeros(matrix.shape)
    matrix_out += matrix  # 先複製一個底圖
    B = matrix_out[:,:,2]
    pos_Bn = B<0
    Bn = np.zeros(matrix.shape)
    Bn[:,:,0] = Bn[:,:,1] =Bn[:,:,2] = pos_Bn.astype(int)*B
    matrix_out -= Bn
    '''
    將R和G都負的地方加到B上面
    將R負處加到G，G負處加到R
    '''
    R = matrix_out[:,:,0]
    G = matrix_out[:,:,1]
    pos_Rn = R<0
    pos_Gn = G<0
    pos_Rn_and_Gn = pos_Rn.astype(int)+pos_Gn.astype(int) == 2  # R負交集G負
    pos_biggerGn = (R-G)*pos_Rn_and_Gn>0
    pos_biggerRn = (R-G)*pos_Rn_and_Gn<0
    Rn_and_Gn =R*pos_biggerRn.astype(int)+G*pos_biggerGn.astype(int)
    Rn = R*(pos_Rn-pos_Rn_and_Gn.astype(int))
    Gn = G*(pos_Gn-pos_Rn_and_Gn.astype(int))
    matrix_out[:,:,0] -= Rn_and_Gn   # 將交集部分加到B上
    matrix_out[:,:,1] -= Rn_and_Gn
    matrix_out[:,:,2] -= Rn_and_Gn
    matrix_out[:,:,1] -= Gn
    matrix_out[:,:,0] -= Gn
    matrix_out[:,:,0] -= Rn
    matrix_out[:,:,1] -= Rn
    return matrix_out


def color_matching_func(begin,end,step):
    data = open("./model/data_set/color_matching_func.txt")
    color_matching_function = []
    counter = 0
    for line in data:
        k = line.replace("\n","").split("\t")
        if counter != 0:
            T = list(map(float,k))
            color_matching_function.append(T)
        counter += 1
    data.close()
    color_matching_function = np.array(color_matching_function)
    out = color_matching_function[begin-380:end-780:step,1:]

    return out


# 參考光源
def select_light_source(light_source):
    if light_source == 'D50':
        light_source = [0.964,1,0.825]
    if light_source == 'D55':
        light_source = [0.957,1,0.825]
    if light_source == 'D65':
        light_source = [0.950,1,1.089]
    if light_source == 'D75':
        light_source = [0.950,1,1.226]
    if light_source == 'A':
        light_source = [1.099,1,0.358]
    if light_source == 'B':
        light_source = [0.991,1,0.853]
    if light_source == 'C':
        light_source = [0.981,1,1.182]
    if light_source == 'E':
        light_source = [1,1,1]

    return np.array(light_source)



def muti_image_to_color_image(dst_folder):
    #色彩轉換矩陣
    im_550 = dst_folder[3][:,:,0]
    h = im_550.shape[0]
    w = im_550.shape[1]
    im_matrix = np.zeros([7,h,w])
    
    for l in range(7):
        im_matrix[l] = dst_folder[l][:,:,0]
    '''
    讀取色匹配函數
    '''
    color_matching_function = color_matching_func(400,700,50)
    
    for i in range(0,7):
        im_matrix[i] /= im_matrix[i][int(2*h/5):int(h*3/5),int(w*2/5):int(3*w/5)].max()
    im_out = np.zeros([im_matrix.shape[1],im_matrix.shape[2],3])
    for j in range(0,7):
        im_out[:,:,0] += im_matrix[j]*color_matching_function[j,0]
        im_out[:,:,1] += im_matrix[j]*color_matching_function[j,1]
        im_out[:,:,2] += im_matrix[j]*color_matching_function[j,2]
    im_out[:,:] /= [im_out[:,:,0].max(),im_out[:,:,1].max(),im_out[:,:,2].max()]
    return im_out


def get_original_image(matrix):
    light_source = select_light_source('E')
    image_original = XYZ2RGB(matrix,light_source)
    image_original = image_original[:, :, ::-1]

    return image_original


# 解碼圖片、讀取圖片
def read_file(image):
    # nparr = np.fromstring(base64.b64decode(image), dtype=np.uint8)
    # source_image = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
    source_image = np.fromstring(image, np.uint8)
    source_image = cv2.imdecode(source_image, cv2.IMREAD_COLOR)
    return source_image

# numpy array 轉 binary
def convert_binary(RGB_image):
    RGB_image = RGB_image.astype(np.uint8)
    _, im_arr = cv2.imencode('.jpg', RGB_image)
    RGB_image = im_arr.tobytes()

    return RGB_image
    

def WB(image_400nm, image_450nm, image_500nm, image_550nm, image_600nm, image_650nm, image_700nm, light_source):
    images_input = [image_400nm, image_450nm, image_500nm, image_550nm, image_600nm, image_650nm, image_700nm]
    images_output = []
    for index in range(len(images_input)):
        # 讀取圖片
        source_image = read_file(images_input[index])  # 轉numpy array 
        images_output.append(source_image)

    # 頻譜7合1
    matrix = muti_image_to_color_image(images_output)
    image_original = get_original_image(matrix)
    # 光源轉換
    light_source = select_light_source(light_source)
    image_light_convert = XYZ2RGB(matrix,light_source)

    # BGR轉RGB
    image_light_convert = image_light_convert[:, :, ::-1]

    # plt.imshow(matrix)
    # plt.show()

    # numpy array 轉 binary
    image_original = convert_binary(image_original)
    image_light_convert = convert_binary(image_light_convert)

    return image_original, image_light_convert


