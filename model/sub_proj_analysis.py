# -*- coding: utf-8 -*-
"""
Created on Wed Aug  5 09:17:05 2020

@author: user
"""

#%%
import numpy as np
#%%
def cut_window(img_Multi,point1,point2):
    #輸入一張影像及兩個座標點，輸出為此影像在兩點所圍成範圍的矩形影像
    h = img_Multi.shape[1]
    w = img_Multi.shape[2]
    y_i = min(point1[0],point2[0])
    y_f = max(point1[0],point2[0])
    x_i = min(point1[1],point2[1])
    x_f = max(point1[1],point2[1])
    if y_i < 0:
        y_i = 0
    if y_f > h:
        y_f = h
    if x_i < 0:
        y_i = 0
    if x_f > h:
        y_f = w
    if y_f-y_i<50:
        y_f = y_i+50
    if x_f-x_i<50:
        x_f = x_i+50
    im_out = img_Multi[:,y_i:y_f,x_i:x_f]
    return im_out

def average_value(img_Multi):
    average = np.zeros([img_Multi.shape[0]])
    for i in range(img_Multi.shape[0]):
        average[i] = np.sum(np.sum(img_Multi[i]))
    average /= img_Multi.shape[1]
    average /= img_Multi.shape[2]
    return average

def color_matching_func(begin,end,step):
    data = open("./data_set/color_matching_func.txt")
    color_matching_function = []
    counter = 0
    for line in data:
        k = line.replace("\n","").split("\t")
        if counter != 0:
            T = list(map(float,k))
            color_matching_function.append(T)
        counter += 1
    data.close()
    color_matching_function = np.array(color_matching_function)
    out = color_matching_function[begin-380:end-780:step,1:]
    return out

def RGB2colorcode(RGB):
    #將RGB輸入，輸出色碼(字串)
    r1 = RGB[0]//16
    r2 = RGB[0]-16*r1
    g1 = RGB[1]//16
    g2 = RGB[1]-16*g1
    b1 = RGB[2]//16
    b2 = RGB[2]-16*b1
    rgb = [r1,r2,g1,g2,b1,b2]
    R1G1B1 = "#"
    for i in rgb:
        if i > 9:
            R1G1B1+=(chr(i-9+64))
        else:
            R1G1B1+=(str(i))
    return R1G1B1

def convert_matrix_build(dataset_A,dataset_B):
    '''
    此函數回傳anti(B)*A = anti(M)中的anti(M)
    之後使用這個矩陣的方法為B*anti(M)=A
    注意，A矩陣的大小要大於B矩陣
    這樣效果會比較好
    舉例:A(24,31)>B(24,7)
    '''
    u,s,v_t =np.linalg.svd(dataset_B)
    diff=s[1:]-s[:-1]
    pos = diff>-0.3
    index= -sum(pos.astype(int))-1
    Sigma = s[:index+1]
    print(Sigma)
    Length = len(Sigma)
    anti_matrix_B = np.dot(v_t[:Length,:].T,1/Sigma*np.eye(Length),u[:,:Length].T)
    anti_convert_Matrix = np.dot(anti_matrix_B,dataset_A)
    return anti_convert_Matrix
    
def color_temp(temp):
    if type(temp)==str:
        if temp[0] == "D":
            temp = int(temp[1:])*100
        elif temp[-1] =="K":
            temp = int(temp[:-1])
        else:
            temp = 6500
            print("input value is illegal!!")
    if 4000<=temp<7000:
        Xd = -4.6070*10**9/temp**3 +2.9678*10**6/temp**2 +0.09911*10**3/temp +0.244063
    elif 7000<=temp<=25000:
        Xd = -2.0064*10**9/temp**3 +1.9018*10**6/temp**2 +0.24748*10**3/temp +0.237040
    else :
        print("input number is illegal")
        Xd = 0.333333
    Yd = -3.000*Xd**2 +2.870*Xd -0.275
    return [Xd,Yd,1-Xd-Yd]
    
    
    
    
    
    
    
