# 專屬雲端頻譜母色資料庫建置開發技術

## 環境配置

- **環境建置，先下載所需檔案**
    1. [Python 3 (version 3.7.7)](https://drive.google.com/drive/folders/1y4PGik5RaNtbw1r0vFALNIcinmg527q9?usp=sharing)
    2. [Xampp (version 8.0.0)](https://drive.google.com/drive/folders/1N6-cTs5v83rTY2vwL92CgfnHRMCcFT5g?usp=sharing)
    3. [DB Schema](https://drive.google.com/drive/folders/1ab1vb92VBjFxFgDl9PwM77i0gqGyuuhs?usp=sharing)

- **資料庫安裝及匯入 Data Base Schema**
    - 教學
        - 安裝好 Xampp 後，進入 C:\xampp\mysql\bin 資料夾內，編輯 my.ini 檔案，將 max_allowed_packet = 1M 改為 <font color="#f00">max_allowed_packet = 16M</font>，完畢後儲存並關閉。
            ![](https://i.imgur.com/imbQznv.jpg)


        - 打開 Xampp 應用程式，點擊 Start 將 Apache 和 MySQL 都啟動。
            ![](https://i.imgur.com/dtAspJU.jpg)




        - 點選 MySQL 的 Admin 按鈕後，開啟 phpmyadmin 頁面。
            ![](https://i.imgur.com/PFmMOSX.jpg)


        - 在 phpmyadmin 中新增一個資料庫， 名稱為 spectrum_db，編碼為 utf8_general_ci。
            ![](https://i.imgur.com/kpDyn9a.jpg)



        - 建立完之後，點擊 spectrum_db 中的匯入，選擇下載好的 spectrum_db.sql 檔案進行匯入。
            ![](https://i.imgur.com/3JSTVTC.jpg)
            
        - 完成 !


- **下載本專案至桌面**
    ###### 1. 使用cmd進入專案資料夾位置
    ```python
    $ cd Desktop/Spectrum cloud database
    ```
    ![](https://i.imgur.com/ap9NYSi.png)
    ###### 2. 安裝python虛擬環境套件
    ```python
    $ pip install virtualenv
    ```
    ![](https://i.imgur.com/Fig6Wjb.png)
    ###### 3. 創建虛擬環境
    ```python
    $ virtualenv venv
    ```
    ![](https://i.imgur.com/WN40fQ5.png)
    ###### 4. 啟動虛擬環境 (成功後，前面會出現 "venv" 的字樣)
    ```python
    $ cd venv/Scripts
    $ activate
    ```
    ![](https://i.imgur.com/8u6jSRP.png)
 

- **安裝 python 套件**

    ```python
    $ pip install Flask
    $ pip install opencv-python
    $ pip install PyMySQL
    $ pip install matplotlib
    ```

- **可自由更改 config.py 檔案中的 IP 與 Port 位置**
    ![](https://i.imgur.com/hZC4ftu.jpg)

- **回到上上層資料夾，並啟動 Server**
    ```python
    $ cd ..
    $ cd ..
    $ python runserver.py
    ```
    ![](https://i.imgur.com/SjGaQTU.png)

---

## 架構圖

![](https://i.imgur.com/g6RyqvH.png)

