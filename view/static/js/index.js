

$(document).ready(function() {
    var API = '/query';
    var dataJSON = {};
    var AllImage = []; // 儲存圖片，用於打包下載圖片
    var ImageName = ['400', '450', '500', '550', '600', '650', '700', 'original', 'convert'];
    $('#download').hide();
    $("#download").attr("disabled", true);

    // 取得資料庫現有的圖片名稱
    $.ajax({
        type: "POST",
        url: '/get_all_image_name',
        dataType: "json",
        success : function(data)
        {
            console.log(data);
            var data_length = data.length;
            if (data_length == 0)
            {
                $('.modal-body').text('目前還沒有任何一筆資料');
                $('#alertModal').modal('show');
            }
            for (var i = 0; i < data_length; i++)
            {
                $('#image_name').append('<li><a class="dropdown-item image_name_dropdown">' + data[i] + '</a></li>')
            }
        }
    });


    // "新增" 按鈕
    $("#add_data").click(function()
    {
        window.location.href = "/uploadr"
    });


    // "頻譜分析" 按鈕
    $("#spectrum_analysis").click(function()
    {
        window.location.href = "/spectrum_analysis"
    });


    // "查詢" 按鈕
    $("#spectrum_query").click(function()
    {
        if (dataJSON['image_name'] == undefined || dataJSON['light_source'] == undefined || dataJSON['degree'] == undefined)
        {
            $('.modal-body').text('輸入欄位不得為空');
            $('#alertModal').modal('show');
        }
        else
        {
            doQuery();
        }
    });


    // 檔案選擇
    $(document).on('click', '.image_name_dropdown', function(event){
        var image_name = $(this).text();
        $('#image').text(image_name);
        dataJSON['image_name'] = image_name;
    });


    // 光源選擇
    $('.light_source_dropdown').click(function()
    {
        var light_source = $(this).text();
        $('#light_source').text(light_source);
        if (light_source == "原圖")
        {
            light_source = 'E'
        }
        dataJSON['light_source'] = light_source;
    });


    // 角度選擇
    $('.degree_dropdown').click(function()
    {
        var degree = $(this).text();
        $('#degree').text(degree);
        dataJSON['degree'] = degree.substring(0,2);
    });


    // 一鍵下載
    $("#download").click(function()
    {
        $("#download").attr("disabled", true);
        $("#download").text("請稍後...");
        var zip = new JSZip();
        for (var i = 0; i<AllImage.length; i++)
        {
            var base64 = AllImage[i].substring(AllImage[i].indexOf(",")+1);
            zip.file(ImageName[i]+".jpg", base64, {base64: true});
        }
        zip.generateAsync({type:"blob"}).then(function (blob) { // 1) generate the zip file
            saveAs(blob, "SpectrumImage.zip");                  // 2) trigger the download
            $("#download").attr("disabled", false);
            $("#download").text("一鍵下載");
        }, function (err) {
            jQuery("#download").text(err);
        });
    });


    // 查詢
    function doQuery()
    {
        $.ajax({
            type :"POST",
            url  : API,
            data : dataJSON,
            dataType: "json",
            beforeSend:function()
            {
                console.log("發送請求中...");
                $('#loadingModal').modal('show');
            },
            success : function(data)
            {
                $('#loadingModal').modal('hide');
                console.log(data[0]);
                if(data[0].status == 'Success'){
                    // 偵測裝置類型
                    if (window.matchMedia("(max-width: 767px)").matches)  
                    { 
                        // The viewport is less than 768 pixels wide 
                        console.log("This is a mobile device.");
                    } else { 
                        
                        // The viewport is at least 768 pixels wide 
                        console.log("This is a tablet or desktop.");
                    } 
                    $('.roll').css('display','block');
                    $('.one').html('400nm <img id=1 class="img-fluid img-thumbnail" src=' + data[0].image_400nm_original + '>');
                    $('.two').html('450nm <img id=2 class="img-fluid img-thumbnail" src=' + data[0].image_450nm_original + '>');
                    $('.three').html('500nm <img id=3 class="img-fluid img-thumbnail" src=' + data[0].image_500nm_original + '>');
                    $('.four').html('550nm <img id=4 class="img-fluid img-thumbnail" src=' + data[0].image_550nm_original + '>');
                    $('.five').html('600nm <img id=5 class="img-fluid img-thumbnail" src=' + data[0].image_600nm_original + '>');
                    $('.six').html('650nm <img id=6 class="img-fluid img-thumbnail" src=' + data[0].image_650nm_original + '>');
                    $('.seven').html('700nm <img id=7 class="img-fluid img-thumbnail" src=' + data[0].image_700nm_original + '>');
                    $('.eight').html('彩色原圖 <img id=8 class="img-fluid img-thumbnail" src=' + data[0].image_original + '>');
                    $('.nine').html('光源轉換 <img id=9 class="img-fluid img-thumbnail" src=' + data[0].image_light_convert + '>');
                    AllImage = [];
                    AllImage.push(data[0].image_400nm_original, data[0].image_450nm_original, data[0].image_500nm_original, data[0].image_550nm_original, data[0].image_600nm_original, data[0].image_650nm_original, data[0].image_700nm_original, data[0].image_original, data[0].image_light_convert);
                    setTimeout(function ()
                    { 
                        $('#download').show();
                        $("#download").attr("disabled", false);
                        console.log("完成");
                    },500);
                }
                else
                {
                    console.log("查無此資料 !!");
                    setTimeout(function ()
                    { 
                        $("#loadingModal").modal("hide");
                    },500);
                    setTimeout(function ()
                    {
                        $('#1').remove();
                        $("#2").remove();
                        $("#3").remove();
                        $("#4").remove();
                        $("#5").remove();
                        $("#6").remove();
                        $("#7").remove();
                        $("#8").remove();
                        $("#9").remove();
                        $('.modal-body').text('查無此資料 !!');
                        $('#alertModal').modal('show');
                    },500);
                }
            },
            complete:function()
            {
                $("#loadingModal").modal("hide");
            }
        });
    }
});


