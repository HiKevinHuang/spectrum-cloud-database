

// 色卡
var canvas = document.getElementById('colorchecker');
var ctx = canvas.getContext("2d");
ctx.fillStyle="blue";
ctx.fillRect(15,25,50,50);


// 光源選擇
$('.light_source_dropdown').click(function()
{
    var light_source = $(this).text();
    $('#light_source').text(light_source);
});


// "分析" 按鈕
$("#analysis").click(function()
{
    alert("功能尚未完成");
});


// 選單一 "儲存"
$("#menu_save_1").click(function()
{
    alert("選單一儲存成功");
});


// 選單二 "儲存"
$("#menu_save_2").click(function()
{
    alert("選單二儲存成功");
});


// 選單三 "儲存"
$("#menu_save_3").click(function()
{
    alert("選單三儲存成功");
});


// 選單四 "儲存"
$("#menu_save_4").click(function()
{
    alert("選單四儲存成功");
});


// amcharts
// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
var chart = am4core.create("chartdiv", am4charts.XYChart);

// Add data
chart.data = generateChartData();

// Create axes
var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
dateAxis.renderer.minGridDistance = 50;

var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

// Create series
var series = chart.series.push(new am4charts.LineSeries());
series.dataFields.valueY = "visits";
series.dataFields.dateX = "date";
series.strokeWidth = 2;
series.minBulletDistance = 10;
series.tooltipText = "{valueY}";
series.tooltip.pointerOrientation = "vertical";
series.tooltip.background.cornerRadius = 20;
series.tooltip.background.fillOpacity = 0.5;
series.tooltip.label.padding(12,12,12,12)

// Add scrollbar
chart.scrollbarX = new am4charts.XYChartScrollbar();
chart.scrollbarX.series.push(series);

// Add cursor
chart.cursor = new am4charts.XYCursor();
chart.cursor.xAxis = dateAxis;
chart.cursor.snapToSeries = series;

function generateChartData() {
    var chartData = [];
    var firstDate = new Date();
    firstDate.setDate(firstDate.getDate() - 1000);
    var visits = 1200;
    for (var i = 0; i < 10; i++) {
        // we create date objects here. In your data, you can have date strings
        // and then set format of your dates using chart.dataDateFormat property,
        // however when possible, use date objects, as this will speed up chart rendering.
        var newDate = new Date(firstDate);
        newDate.setDate(newDate.getDate() + i);
        
        visits += Math.round((Math.random()<0.5?1:-1)*Math.random()*10);

        chartData.push({
            date: newDate,
            visits: visits
        });
    }
    return chartData;
}