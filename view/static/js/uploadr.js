


var image400;
var image450;
var image500;
var image550;
var image600;
var image650;
var image700;
var degree;
var ImageName = ['400', '450', '500', '550', '600', '650', '700'];
var API = "/AddData";

// 下拉式選單
$('.dropdown-item').click(function()
{
    degree = $(this).text();
    $('#dropdown').text(degree);
    degree = degree.substring(0,2);  // 去除"度"的符號
});


// 送出按鈕
$("#add_data").click(function()
{
    var image_name_input = $( "#image_name_input" ).val();
    if ((image_name_input == '') || (typeof degree === "undefined") || (typeof image400 === "undefined") || (typeof image450 === "undefined") || (typeof image500 === "undefined") || (typeof image550 === "undefined") || (typeof image600 === "undefined") || (typeof image650 === "undefined") || (typeof image700 === "undefined"))
    {
        $('#yes').attr('id','know');
        $('.modal-body').text('輸入欄位不得為空');
        $('#no').remove();
        $('#alertModal').modal('show');
    }
    else
    {
        var data = new FormData(data);
        data.append('image_name', image_name_input);
        data.append('degree', degree);
        data.append('400nm', image400);
        data.append('450nm', image450);
        data.append('500nm', image500);
        data.append('550nm', image550);
        data.append('600nm', image600);
        data.append('650nm', image650);
        data.append('700nm', image700);
        data.append('mode', 'Add');
        doQuery(data)
    }
});

// 圖片選擇
$("#FileInput_400").change(function()
{
    readURL(this, function(e){
        $('#one').html('<img id=1 class="img-fluid img-thumbnail" src=' + e.target.result + '>');
        image400 = e.target.result.split(',');
        image400 = image400[1];
    });
});
$("#FileInput_450").change(function()
{
    readURL(this, function(e){
        $('#two').html('<img id=2 class="img-fluid img-thumbnail" src=' + e.target.result + '>');
        image450= e.target.result.split(',');
        image450 = image450[1];
    });
});
$("#FileInput_500").change(function()
{
    readURL(this, function(e){
        $('#three').html('<img id=3 class="img-fluid img-thumbnail" src=' + e.target.result + '>');
        image500 = e.target.result.split(',');
        image500 = image500[1];
    });
});
$("#FileInput_550").change(function()
{
    readURL(this, function(e){
        $('#four').html('<img id=4 class="img-fluid img-thumbnail" src=' + e.target.result + '>');
        image550 = e.target.result.split(',');
        image550 = image550[1];
    });
});
$("#FileInput_600").change(function()
{
    readURL(this, function(e){
        $('#five').html('<img id=5 class="img-fluid img-thumbnail" src=' + e.target.result + '>');
        image600 = e.target.result.split(',');
        image600 = image600[1];
    });
});
$("#FileInput_650").change(function()
{
    readURL(this, function(e){
        $('#six').html('<img id=6 class="img-fluid img-thumbnail" src=' + e.target.result + '>');
        image650 = e.target.result.split(',');
        image650 = image650[1];
    });
});
$("#FileInput_700").change(function()
{
    readURL(this, function(e){
        $('#seven').html('<img id=7 class="img-fluid img-thumbnail" src=' + e.target.result + '>');
        image700 = e.target.result.split(',');
        image700 = image700[1];
    });
});

function readURL(input, onLoadCallback)
{
    if(input.files && input.files[0])
    {
        var reader = new FileReader();
        reader.onload = onLoadCallback;
        reader.readAsDataURL(input.files[0]);
    }
}


// 傳送資料
function doQuery(data)
{
    $.ajax({
        type :"POST",
        url  : API,
        data : data,
        cache: false,
        // async : false,
        processData: false,
        contentType: false,
        beforeSend:function()
        {
            console.log("發送請求中...");
            $('#loadingModal').modal('show');
        },
        success : function(data) {
            if (data == "Success")
            {
                setTimeout(function ()
                { 
                    $("#loadingModal").modal("hide");
                },500);
                // alert("新增成功");
                setTimeout(function ()
                { 
                    $('.modal-body').text('新增成功');
                    $('#alertModal').modal('show');
                },500);
            }
            else if (data == "Failure")
            {
                // alert("新增失敗");
                setTimeout(function ()
                { 
                    $("#loadingModal").modal("hide");
                },500);
                setTimeout(function ()
                { 
                    $('.modal-body').text('新增失敗');
                    $('#alertModal').modal('show');
                },500);
            }
            else
            {
                setTimeout(function ()
                { 
                    $("#loadingModal").modal("hide");
                },500);
                setTimeout(function ()
                { 
                    $('.modal-body').text(data);
                    $('#know').attr('id','yes');
                    $('#yes').after('<button id="no" type="button" class="btn btn-primary" data-dismiss="modal">取消</button>');
                    $('#alertModal').modal('show');
                },500);
            }
        },

    });
}

$("#footer").on('click','#yes',function(){
    var data = new FormData(data);
    var image_name_input = $( "#image_name_input" ).val();
    data.append('image_name', image_name_input);
    data.append('degree', degree);
    data.append('400nm', image400);
    data.append('450nm', image450);
    data.append('500nm', image500);
    data.append('550nm', image550);
    data.append('600nm', image600);
    data.append('650nm', image650);
    data.append('700nm', image700);
    data.append('mode', 'Update');
    data['mode'] = 'Update';
    doQuery(data)
    $('#yes').attr('id','know');
    $('#yes').text('知道了');
    $('#no').remove();
});
$("#footer").on('click','#no',function(){
    $('#alertModal').modal('hide');
    $("#image_name_input").val("");
    $('#dropdown').text("角度選擇");
    for (var i = 0; i<=7; i++)
    {
        $("#"+i.toString()).remove();
        $("#FileInput_"+ImageName[i]).val(null);
    }
});
